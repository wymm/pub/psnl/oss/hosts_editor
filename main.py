import toml
from editor import replace, append, rewrite

# Read from config file
with open('config.toml', 'r') as f:
    config = toml.loads(f.read())
hosts_path = config['Hosts']
vhosts_path = config['Vhosts']
hosts_template = config['HostsTemplate']
vhosts_template = config['VhostsTemplate']

# UI / Get Values
print('Welcome to hosts_editor!')

# Check for admin rights
try:
    path = r'C:\Windows\System32\drivers\etc\testAdminRights.txt'
    with open(path, 'w+') as f:
        f.write('Admin permission is granted')
    import os
    os.remove(path)
except PermissionError:
    print('[Warning] You need admin permission in order to edit hosts file')
    inp = input("Enter anything to quit: ")
    sys.exit()

svr_name = input('Please enter your domain name: ')
if svr_name == '':
    svr_name = 'example.com'
doc_root = input('Please enter your document root: ')
if doc_root == '':
    doc_root = 'C:/'
port_num = input('Please enter your port number (Leave empty for 80): ')
if port_num == '':
    port_num = '80'


print("\nVerify these path locations")
print(f"Hosts Path is {hosts_path}")
print(f"Vhosts Path is {vhosts_path}")
input("Press enter to continue...")
print('\n')

# Replacements and save to disk
append(hosts_path, replace(hosts_template, {'SvrName': svr_name}), "custom url for xampp localhost", "down")
append(vhosts_path, replace(vhosts_template, {'PortNum': port_num, 'SvrName': svr_name, 'DocRoot': doc_root}))

print('Operations completed. Have a nice day!')