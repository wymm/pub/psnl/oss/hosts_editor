def replace(content, replacement):
    for old, new in replacement.items():
        content = content.replace(old, new)
    return content


def append(file_path, content, match='', location='down'):
    # Validate input
    valid_locations = ['up', 'down']
    if location not in valid_locations:
        raise ValueError('location must be either "up" or "down"')

    with open(file_path, 'r') as f:
        lines = f.readlines()

    # if a string is matched
    #   location describe where the content will be in relative to the string's line
    # if a string is not matched or it is empty
    #   location describe where it is in the entire file
    # NOTE: Only the first match from top is counted, rest is ignored

    i = 0
    matched = False
    if match != "":
        while i < len(lines) and not matched:
            line = lines[i]
            if match in line:
                matched = True
                if location == 'up':
                    location_index = i
                elif location == 'down':
                    location_index = i + 1
            i += 1

    if not matched:
        if location == 'up':
            location_index = 0
        elif location == 'down':
            location_index = len(lines)

    if location_index == 0:
        content += '\n'
    elif location_index == len(lines):
        content = '\n' + content
    elif location == 'down':
        content += '\n'
    elif location == 'up':
        content = '\n' + content

    lines.insert(location_index, content)
    with open(file_path, 'w') as f:
        f.writelines(lines)


class editor:
    content = None
    replacement = None
    path = None
    match = None
    location = None

    def __init__(self, content, replacement = dict(), path = None, match = '', location = "down"):
        self.content = content
        self.replacement = replacement
        self.path = path
        self.match = match
        self.location = location

    def __str__(self):
        return str(self.content) + str(self.replacement)

    def replace(self, content = None, replacement = None):
        if content == None:
            content = self.content
        if replacement == None:
            replacement = self.replacement

        return replace(content, replacement)

    def replace_self(self, content = None, replacement = None):
        self.content = self.replace(content, replacement)
        return self

    def append(self, path = None, match = None, location = None, content = None):
        if content == None:
            content = self.content
        if path == None:
            path = self.path
        if match == None:
            match = self.match
        if location == None:
            location = self.location

        append(path, content, match, location)